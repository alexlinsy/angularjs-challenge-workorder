/******
*This file includes fucntions of the page
*Get RESTapi Json data
*Sort works by date
******/
var app = angular.module('myApp', []);

/**
*Inject addtional service to get workers' data by asynchromous
*@param id the id of worker
**/
app.service('workersService', function($http) {
    this.getWorker = function(id) {
      return $http.get('https://www.hatchways.io/api/assessment/workers/' + id);
    }
});

app.controller('contentCtrl', ['$scope', '$http', 'workersService', function($scope, $http, workersService) {
  let workers  = [];
  let allDates = [];

  /**
  *Get work orders data by using $http service in asynchromous way
  *@param response is retrieved data objects
  **/
  let promise = $http.get('https://www.hatchways.io/api/assessment/work_orders');
  promise.then(function success(response) {
    $scope.works = response.data.orders;
  }, function error(response) {
    alert("Retrieved data failed");
  });

  /**
  *Get workers' data by using $http service in asynchromous way
  *@param response is retrieved data objects
  **/
  for(let id = 0; id <= 4; id++) {
    let promise = workersService.getWorker(id);
    promise.then(function success(response) {
      workers.push(response.data.worker);
    }, function error(response) {
      alert("Retrieved data failed");
    });
  }

  $scope.workers = workers;
  //Sort works by date
  $scope.orderProperty = '+deadline';
  /**
  *Sort works by chaning the property of  angularjs 'orderBy'
  *@param propertyName is string of the property of angularjs 'orderBy' 
  **/
  $scope.sortBy = function(propertyName) {
    if($scope.checkBox) {
      $scope.orderProperty = '+' + propertyName;
    } else {
      $scope.orderProperty = '-' + propertyName;
    }
  }
}]);
