/******
  This file handles the directive of the page elements
*******/
var app = angular.module('myApp');

app.directive('contentView', function() {
  return {
    restrict: 'A',
    templateUrl: '../html/contentView.html',
    replace: true
  }
});
